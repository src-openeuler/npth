Name:           npth
Version:        1.8
Release:        1
Summary:        The New GNU Portable Threads library
License:        LGPL-2.1-or-later
URL:            https://gnupg.org/software/npth/index.html
Source:         https://gnupg.org/ftp/gcrypt/npth/%{name}-%{version}.tar.bz2
Patch0:         add-test-cases.patch

BuildRequires:  make gcc
BuildRequires:  autoconf automake libtool

%description
The NPth package contains a very portable POSIX/ANSI-C based 
library for Unix platforms which provides non-preemptive 
priority-based scheduling for multiple threads of execution 
(multithreading) inside event-driven applications. All threads 
run in the same address space of the server application, but 
each thread has its own individual program-counter, run-time 
stack, signal mask and errno variable.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
This package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -fi
%configure --disable-static
%make_build

%install
%make_install
%{?delete_la}

%check
%make_build check

%files
%license COPYING.LIB
%{_libdir}/lib%{name}.so.*

%files devel
%doc AUTHORS ChangeLog NEWS README
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/%{name}.h
%{_datadir}/aclocal/%{name}.m4

%changelog
* Fri Nov 29 2024 Funda Wang <fundawang@yeah.net> - 1.8-1
- update to 1.8

* Sat Jan 28 2023 licihua<licihua@huawei.com> - 1.6-7
- DESC: modify the patchs location

* Sat Sep 24 2022 yaowenbin1 <yaowenbin1@huawei.com> - 1.6-6
- DESC: fix t-socket test case fail

* Thu Mar 10 2022 EulerOSWander <314264452@qq.com> - 1.6-5
- DESC: posix: Add npth_poll/npth_ppoll

* Tue Mar 1 2022 yaowenbinHW <yaowenbin@huawei.com> - 1.6-4
- DESC: add test cases

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.6-3
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Mon Sep 28 2020 EulerOSWander <314264452@qq.com> - 1.6-2
- Use cast by uintptr_t for thread ID.

* Thu Jul 23 2020 jinzhimin <jinzhimin2@huawei.com> - 1.6-1
- Package update

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.5-7
- Package init
